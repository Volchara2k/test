package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;

public interface IEndpointContextService {

    @NotNull
    AuthenticationEndpoint getAuthenticationEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    AdminDataInterChangeEndpoint getAdminDataInterChangeEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

}