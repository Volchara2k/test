package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceContextService {

    @NotNull
    ICurrentSessionService getCurrentSession();

    @NotNull
    ICommandService getCommandService();

}