package ru.renessans.jvschool.volkov.task.manager.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class UserLockCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_USER_LOCK = "user-lock";

    @NotNull
    private static final String DESC_USER_LOCK = "заблокировать пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_LOCK =
            "Происходит попытка инициализации блокирования пользователя. \n" +
                    "Для блокирования пользователя введите его логин. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_USER_LOCK;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_USER_LOCK;
    }

    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final AdminEndpoint adminEndpoint = endpointLocator.getAdminEndpoint();

        ViewUtil.print(NOTIFY_USER_LOCK);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final UserLimitedDTO user = adminEndpoint.lockUserByLogin(current, login);
        ViewUtil.print(user);
    }

}