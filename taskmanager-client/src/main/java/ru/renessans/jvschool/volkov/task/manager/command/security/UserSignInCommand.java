package ru.renessans.jvschool.volkov.task.manager.command.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class UserSignInCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_SIGN_IN = "sign-in";

    @NotNull
    private static final String DESC_SIGN_IN = "войти в систему";

    @NotNull
    private static final String NOTIFY_SIGN_IN =
            "Происходит попытка инициализации авторизации пользователя. \n" +
                    "Для авторизации пользователя в системе введите логин и пароль: ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_SIGN_IN;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_SIGN_IN;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_SIGN_IN);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final String password = ViewUtil.getLine();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();
        @NotNull final SessionDTO open = sessionEndpoint.openSession(login, password);

        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();

        @NotNull final SessionDTO subscribeSession = currentSessionService.subscribe(open);
        ViewUtil.print(subscribeSession);
    }

}