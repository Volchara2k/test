package ru.renessans.jvschool.volkov.task.manager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class TaskDeleteByIdCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_TASK_DELETE_BY_ID = "task-delete-by-id";

    @NotNull
    private static final String DESC_TASK_DELETE_BY_ID = "удалить задачу по идентификатору";

    @NotNull
    private static final String NOTIFY_TASK_DELETE_BY_ID =
            "Происходит попытка инициализации удаления задачи. \n" +
                    "Для удаления задачи по идентификатору введите идентификатор задачи из списка. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_DELETE_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_DELETE_BY_ID;
    }

    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_TASK_DELETE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @Nullable final TaskDTO task = taskEndpoint.deleteTaskById(current, id);
        ViewUtil.print(task);
    }

}