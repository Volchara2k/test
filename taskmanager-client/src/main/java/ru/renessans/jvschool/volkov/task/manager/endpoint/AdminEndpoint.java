package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-31T17:48:51.237+04:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "AdminEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getAllUsersTasksRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getAllUsersTasksResponse")
    @RequestWrapper(localName = "getAllUsersTasks", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllUsersTasks")
    @ResponseWrapper(localName = "getAllUsersTasksResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllUsersTasksResponse")
    @WebResult(name = "tasks", targetNamespace = "")
    public java.util.List<ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO> getAllUsersTasks(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/updatePasswordByIdRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/updatePasswordByIdResponse")
    @RequestWrapper(localName = "updatePasswordById", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.UpdatePasswordById")
    @ResponseWrapper(localName = "updatePasswordByIdResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.UpdatePasswordByIdResponse")
    @WebResult(name = "updatedUser", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO updatePasswordById(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id,
            @WebParam(name = "newPassword", targetNamespace = "")
                    java.lang.String newPassword
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getUserByLoginRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getUserByLoginResponse")
    @RequestWrapper(localName = "getUserByLogin", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetUserByLogin")
    @ResponseWrapper(localName = "getUserByLoginResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetUserByLoginResponse")
    @WebResult(name = "user", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO getUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/setAllUsersProjectsRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/setAllUsersProjectsResponse")
    @RequestWrapper(localName = "setAllUsersProjects", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.SetAllUsersProjects")
    @ResponseWrapper(localName = "setAllUsersProjectsResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.SetAllUsersProjectsResponse")
    public void setAllUsersProjects(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(mode = WebParam.Mode.INOUT, name = "projects", targetNamespace = "")
                    javax.xml.ws.Holder<java.util.List<ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO>> projects
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/closedAllSessionsRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/closedAllSessionsResponse")
    @RequestWrapper(localName = "closedAllSessions", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.ClosedAllSessions")
    @ResponseWrapper(localName = "closedAllSessionsResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.ClosedAllSessionsResponse")
    @WebResult(name = "closedAllSessions", targetNamespace = "")
    boolean closedAllSessions(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/lockUserByLoginRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/lockUserByLoginResponse")
    @RequestWrapper(localName = "lockUserByLogin", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.LockUserByLogin")
    @ResponseWrapper(localName = "lockUserByLoginResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.LockUserByLoginResponse")
    @WebResult(name = "lockedUser", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO lockUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/serverDataRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/serverDataResponse")
    @RequestWrapper(localName = "serverData", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.ServerData")
    @ResponseWrapper(localName = "serverDataResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.ServerDataResponse")
    @WebResult(name = "serverData", targetNamespace = "")
    public java.lang.String serverData(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO arg0
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getUserByIdRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getUserByIdResponse")
    @RequestWrapper(localName = "getUserById", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetUserById")
    @ResponseWrapper(localName = "getUserByIdResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetUserByIdResponse")
    @WebResult(name = "user", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO getUserById(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/unlockUserByLoginRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/unlockUserByLoginResponse")
    @RequestWrapper(localName = "unlockUserByLogin", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.UnlockUserByLogin")
    @ResponseWrapper(localName = "unlockUserByLoginResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.UnlockUserByLoginResponse")
    @WebResult(name = "unlockedUser", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO unlockUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getAllUsersRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getAllUsersResponse")
    @RequestWrapper(localName = "getAllUsers", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllUsers")
    @ResponseWrapper(localName = "getAllUsersResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllUsersResponse")
    @WebResult(name = "users", targetNamespace = "")
    public java.util.List<ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO> getAllUsers(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/deleteUserByLoginRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/deleteUserByLoginResponse")
    @RequestWrapper(localName = "deleteUserByLogin", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteUserByLogin")
    @ResponseWrapper(localName = "deleteUserByLoginResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteUserByLoginResponse")
    @WebResult(name = "deletedUser", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO deleteUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getAllSessionsRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getAllSessionsResponse")
    @RequestWrapper(localName = "getAllSessions", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllSessions")
    @ResponseWrapper(localName = "getAllSessionsResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllSessionsResponse")
    @WebResult(name = "sessions", targetNamespace = "")
    public java.util.List<ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO> getAllSessions(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getAllUsersProjectsRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/getAllUsersProjectsResponse")
    @RequestWrapper(localName = "getAllUsersProjects", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllUsersProjects")
    @ResponseWrapper(localName = "getAllUsersProjectsResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllUsersProjectsResponse")
    @WebResult(name = "projects", targetNamespace = "")
    public java.util.List<ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO> getAllUsersProjects(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/editProfileByIdRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/editProfileByIdResponse")
    @RequestWrapper(localName = "editProfileById", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.EditProfileById")
    @ResponseWrapper(localName = "editProfileByIdResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.EditProfileByIdResponse")
    @WebResult(name = "editedUser", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO editProfileById(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id,
            @WebParam(name = "firstName", targetNamespace = "")
                    java.lang.String firstName
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/deleteUserByIdRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/deleteUserByIdResponse")
    @RequestWrapper(localName = "deleteUserById", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteUserById")
    @ResponseWrapper(localName = "deleteUserByIdResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteUserByIdResponse")
    @WebResult(name = "deletedUser", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO deleteUserById(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/signUpUserWithUserRoleRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/signUpUserWithUserRoleResponse")
    @RequestWrapper(localName = "signUpUserWithUserRole", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.SignUpUserWithUserRole")
    @ResponseWrapper(localName = "signUpUserWithUserRoleResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.SignUpUserWithUserRoleResponse")
    @WebResult(name = "user", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO signUpUserWithUserRole(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password,
            @WebParam(name = "userRole", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.UserRole userRole
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/setAllUsersRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/setAllUsersResponse")
    @RequestWrapper(localName = "setAllUsers", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.SetAllUsers")
    @ResponseWrapper(localName = "setAllUsersResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.SetAllUsersResponse")
    public void setAllUsers(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(mode = WebParam.Mode.INOUT, name = "users", targetNamespace = "")
                    javax.xml.ws.Holder<java.util.List<ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO>> users
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/editProfileByIdWithLastNameRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/editProfileByIdWithLastNameResponse")
    @RequestWrapper(localName = "editProfileByIdWithLastName", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.EditProfileByIdWithLastName")
    @ResponseWrapper(localName = "editProfileByIdWithLastNameResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.EditProfileByIdWithLastNameResponse")
    @WebResult(name = "editedUser", targetNamespace = "")
    public ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO editProfileByIdWithLastName(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id,
            @WebParam(name = "firstName", targetNamespace = "")
                    java.lang.String firstName,
            @WebParam(name = "lastName", targetNamespace = "")
                    java.lang.String lastName
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/deleteAllUsersRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/deleteAllUsersResponse")
    @RequestWrapper(localName = "deleteAllUsers", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteAllUsers")
    @ResponseWrapper(localName = "deleteAllUsersResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteAllUsersResponse")
    @WebResult(name = "deletedUsers", targetNamespace = "")
    public boolean deleteAllUsers(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/setAllUsersTasksRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/AdminEndpoint/setAllUsersTasksResponse")
    @RequestWrapper(localName = "setAllUsersTasks", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.SetAllUsersTasks")
    @ResponseWrapper(localName = "setAllUsersTasksResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.SetAllUsersTasksResponse")
    public void setAllUsersTasks(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO session,
            @WebParam(mode = WebParam.Mode.INOUT, name = "tasks", targetNamespace = "")
                    javax.xml.ws.Holder<java.util.List<ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO>> tasks
    );
}
