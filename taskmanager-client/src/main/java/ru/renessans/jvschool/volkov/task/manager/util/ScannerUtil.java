package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;

import java.util.Scanner;

@UtilityClass
public final class ScannerUtil {

    @NotNull
    private static final Scanner SCANNER = new Scanner(System.in);

    @NotNull
    public String getLine() {
        return SCANNER.next();
    }

    @NotNull
    @SneakyThrows
    public Integer getInteger() {
        @NotNull final String integerLine = getLine();
        try {
            return Integer.parseInt(integerLine);
        } catch (@NotNull final Exception e) {
            throw new IllegalIndexException(integerLine);
        }
    }

}