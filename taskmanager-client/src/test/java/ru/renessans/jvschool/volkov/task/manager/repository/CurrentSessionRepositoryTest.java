package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.EmptySessionException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;

import java.util.UUID;

public final class CurrentSessionRepositoryTest {

    @NotNull
    private final ICurrentSessionRepository currentSessionRepository = new CurrentSessionRepository();

    @Test(expected = EmptySessionException.class)
    @TestCaseName("Run testNegativeDelete for delete()")
    @Category({NegativeImplementation.class, RepositoryImplementation.class})
    public void testNegativeDelete() {
        Assert.assertNotNull(this.currentSessionRepository);
        this.currentSessionRepository.delete();
    }

    @Test
    @TestCaseName("Run testPut for put(session)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testPut() {
        Assert.assertNotNull(this.currentSessionRepository);
        @NotNull final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());

        @NotNull final SessionDTO putSession = this.currentSessionRepository.put(session);
        Assert.assertNotNull(putSession);
        Assert.assertEquals(session.getId(), putSession.getId());
        Assert.assertEquals(session.getTimestamp(), putSession.getTimestamp());
        Assert.assertEquals(session.getUserId(), putSession.getUserId());
    }

    @Test
    @TestCaseName("Run testGet for get()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGet() {
        Assert.assertNotNull(this.currentSessionRepository);
        @NotNull final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());
        @NotNull final SessionDTO putSession = this.currentSessionRepository.put(session);
        Assert.assertNotNull(putSession);

        @Nullable final SessionDTO getSession = this.currentSessionRepository.get();
        Assert.assertNotNull(getSession);
        Assert.assertEquals(putSession.getId(), getSession.getId());
        Assert.assertEquals(putSession.getTimestamp(), getSession.getTimestamp());
        Assert.assertEquals(putSession.getUserId(), getSession.getUserId());
        Assert.assertEquals(putSession.getSignature(), getSession.getSignature());
    }

    @Test
    @TestCaseName("Run testDelete for delete()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testDelete() {
        Assert.assertNotNull(this.currentSessionRepository);
        @NotNull final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());
        @NotNull final SessionDTO putSession = this.currentSessionRepository.put(session);
        Assert.assertNotNull(putSession);

        @Nullable final SessionDTO deleteSession = this.currentSessionRepository.delete();
        Assert.assertNotNull(deleteSession);
        Assert.assertEquals(putSession.getId(), deleteSession.getId());
        Assert.assertEquals(putSession.getTimestamp(), deleteSession.getTimestamp());
        Assert.assertEquals(putSession.getUserId(), deleteSession.getUserId());
        Assert.assertEquals(putSession.getSignature(), deleteSession.getSignature());
    }

}