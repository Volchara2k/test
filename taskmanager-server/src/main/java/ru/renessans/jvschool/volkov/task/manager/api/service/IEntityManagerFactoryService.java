package ru.renessans.jvschool.volkov.task.manager.api.service;

import javax.persistence.EntityManager;

public interface IEntityManagerFactoryService {

    void build();

    EntityManager getEntityManager();

}