package ru.renessans.jvschool.volkov.task.manager.api.service.adapter;

import ru.renessans.jvschool.volkov.task.manager.dto.UserUnlimitedDTO;

public interface IUserUnlimitedAdapterService extends IUserAdapterService<UserUnlimitedDTO> {
}