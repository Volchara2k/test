package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalTransactionException extends AbstractException {

    @NotNull
    private static final String TRANSACTION_ILLEGAL =
            "Ошибка! Выполнение транзакции завершилось нелегальным образом!\n";

    public IllegalTransactionException(@NotNull final Throwable cause) {
        super(TRANSACTION_ILLEGAL, cause);
    }

}