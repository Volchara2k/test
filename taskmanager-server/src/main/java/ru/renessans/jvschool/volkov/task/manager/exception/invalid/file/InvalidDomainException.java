package ru.renessans.jvschool.volkov.task.manager.exception.invalid.file;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidDomainException extends AbstractException {

    @NotNull
    private static final String EMPTY_DOMAIN = "Ошибка! Параметр \"домен\" является null!\n";

    public InvalidDomainException() {
        super(EMPTY_DOMAIN);
    }

}