package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IOwnerUserRepository;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTaskException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractUserOwner;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Objects;

public abstract class AbstractUserOwnerRepository<E extends AbstractUserOwner> extends AbstractRepository<E> implements IOwnerUserRepository<E> {

    @NotNull
    private final EntityManager entityManager;

    public AbstractUserOwnerRepository(
            @NotNull final EntityManager entityManager
    ) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public E persist(
            @NotNull final E value
    ) {
        @NotNull final User user = this.entityManager.find(User.class, value.getUserId());
        value.setUser(user);
        return super.persist(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E deleteByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final E value = getByIndex(userId, index);
        if (Objects.isNull(value)) throw new InvalidTaskException();
        this.entityManager.remove(value);
        return value;
    }

    @NotNull
    @SneakyThrows
    @Override
    public E deleteById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final E value = getById(userId, id);
        if (Objects.isNull(value)) throw new InvalidTaskException();
        this.entityManager.remove(value);
        return value;
    }

    @NotNull
    @SneakyThrows
    @Override
    public E deleteByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @Nullable final E value = getByTitle(userId, title);
        if (Objects.isNull(value)) throw new InvalidTaskException();
        this.entityManager.remove(value);
        return value;
    }

    @NotNull
    @Override
    public Collection<E> deleteAll(
            @NotNull final String userId
    ) {
        @Nullable final Collection<E> values = getAll(userId);
        values.forEach(this.entityManager::remove);
        return values;
    }

}