package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IAdapterContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.service.*;
import ru.renessans.jvschool.volkov.task.manager.service.adapter.AdapterContextService;

public final class ServiceContextRepository implements IServiceContextRepository {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConfigurationService configService = new ConfigurationService(propertyService);

    @NotNull
    private final IEntityManagerFactoryService managerFactoryService = new EntityManagerFactoryService(configService);

    @NotNull
    private final IAdapterContextRepository adapterRepository = new AdapterContextRepository();

    @NotNull
    private final IAdapterContextService adapterService = new AdapterContextService(adapterRepository);


    @NotNull
    private final ITaskUserService taskService = new TaskUserService(managerFactoryService);

    @NotNull
    private final IProjectUserService projectService = new ProjectUserService(managerFactoryService);

    @NotNull
    private final IUserService userService = new UserService(managerFactoryService);

    @NotNull
    private final IAuthenticationService authService = new AuthenticationService(userService);

    @NotNull
    private final ISessionService sessionService = new SessionService(
            managerFactoryService, authService, userService, configService
    );


    @NotNull
    private final IDomainService domainService = new DomainService(
            userService, taskService, projectService, adapterService
    );

    @NotNull
    private final IDataInterChangeService dataService = new DataInterChangeService(
            configService, domainService
    );

    @NotNull
    @Override
    public IUserService getUserService() {
        return this.userService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return this.sessionService;
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return this.authService;
    }

    @NotNull
    @Override
    public ITaskUserService getTaskService() {
        return this.taskService;
    }

    @NotNull
    @Override
    public IProjectUserService getProjectService() {
        return this.projectService;
    }

    @NotNull
    @Override
    public IDataInterChangeService getDataInterChangeService() {
        return this.dataService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return this.propertyService;
    }

    @NotNull
    @Override
    public IConfigurationService getConfigurationService() {
        return this.configService;
    }

    @NotNull
    @Override
    public IEntityManagerFactoryService getEntityManagerService() {
        return this.managerFactoryService;
    }

    @NotNull
    @Override
    public IAdapterContextService getAdapterService() {
        return this.adapterService;
    }

}