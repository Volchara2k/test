package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ISessionRepository;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public Session persist(@NotNull final Session session) {
        @NotNull final User user = this.entityManager.find(User.class, session.getUserId());
        session.setUser(user);
        return super.persist(session);
    }

    @Nullable
    @Override
    public Session getById(@NotNull final String id) {
        @NotNull final String jpqlQuery = "FROM Session WHERE id = :id";
        @NotNull final List<Session> listSessions = this.entityManager.createQuery(jpqlQuery, Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (listSessions.isEmpty()) return null;
        return listSessions.get(0);
    }

    @Override
    public boolean containsUserId(@NotNull final String userId) {
        @Nullable final Session session = getSessionByUserId(userId);
        return !Objects.isNull(session);
    }

    @Nullable
    @Override
    public Session getSessionByUserId(@NotNull final String userId) {
        @NotNull final String jpqlQuery = "FROM Session WHERE userId = :userId";
        @NotNull final List<Session> sessions = this.entityManager.createQuery(jpqlQuery, Session.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList();
        if (sessions.isEmpty()) return null;
        return sessions.get(0);
    }

    @NotNull
    @Override
    public Collection<Session> getAllRecords() {
        return this.entityManager.createQuery("FROM Session", Session.class)
                .getResultList();
    }

}